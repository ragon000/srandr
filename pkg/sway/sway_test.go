package sway
import "testing"

func TestEqualOutput(t *testing.T){
  o := Output{
    Name: "asdf",
    Make: "ass",
    Model: "asdf",
    Serial: "2",
    Rect: Rectangle{X:0,Y:0,Width:0,Height:0},
  }
  p := Output{
    Name: "asadf",
    Make: "asds",
    Model: "asedf",
    Serial: "3",
  }

  got := o.IsEqualTo(o)
  if got == false {
    t.Errorf("Output isEqualTo with same outputs fails")
  }
  got = o.IsEqualTo(p)
  if got == true {
    t.Errorf("Output isEqualTo with different outputs fails")
  }


}

func TestEqualMode(t *testing.T){
  o := Mode{ Width: 1, Height: 1, Refresh: 4}
  p := Mode{ Width: 2, Height: 1, Refresh: 4}
  got := o.IsEqualTo(o)
  if got == false {
    t.Errorf("Mode isEqualTo with same outputs fails")
  }
  got = o.IsEqualTo(p)
  if got == true {
    t.Errorf("Mode isEqualTo with different outputs fails")
  }
}

func TestUpDownRightLeft(t *testing.T){
  o := make([]Output, 3)
  o[0] = Output{
    Name: "asdf",
    Make: "ass",
    Model: "asdf",
    Serial: "2",
    Rect: Rectangle{X:0,Y:0,Width:0,Height:0},
  }
  o[1] = Output{
    Name: "asdf",
    Make: "ass",
    Model: "asdf",
    Serial: "2",
    Rect: Rectangle{X:1,Y:0,Width:0,Height:0},
  }
  o[2] = Output{
    Name: "asdf",
    Make: "ass",
    Model: "asdf",
    Serial: "2",
    Rect: Rectangle{X:0,Y:1,Width:0,Height:0},
  }
  got := UpOf(&o[0], o)
  if !got.IsEqualTo(o[0]) {
    t.Errorf("UpOf with First output fails")
  }
  got = DownOf(&o[1], o)
  if !got.IsEqualTo(o[1]) {
    t.Errorf("DownOf with Last output fails")
  }
  got = LeftOf(&o[0], o)
  if !got.IsEqualTo(o[0]) {
    t.Errorf("LeftOf with First output fails")
  }
  got = RightOf(&o[2], o)
  if !got.IsEqualTo(o[2]) {
    t.Errorf("RightOf with Last output fails")
  }
  got = UpOf(&o[1], o)
  if !got.IsEqualTo(o[0]) {
    t.Errorf("UpOf with Last output fails")
  }
  got = DownOf(&o[0], o)
  if !got.IsEqualTo(o[1]) {
    t.Errorf("DownOf with First output fails")
  }
  got = LeftOf(&o[2], o)
  if !got.IsEqualTo(o[0]) {
    t.Errorf("LeftOf with Last output fails")
  }
  got = RightOf(&o[0], o)
  if !got.IsEqualTo(o[2]) {
    t.Errorf("RightOf with First output fails")
  }
}
