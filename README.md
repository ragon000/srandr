# srandr - a tool to manage your sway (and maybe X in the future) screens
![AUR version](https://img.shields.io/aur/version/srandr?style=flat-square) ![AUR votes](https://img.shields.io/aur/votes/srandr?style=flat-square) ![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/ragon000/srandr?style=flat-square)
![demo image](https://gitlab.com/ragon000/srandr/raw/master/demo.webp?inline=false)

## Installation

```
$ go get gitlab.com/ragon000/srandr
```

