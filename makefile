## Based on https://about.gitlab.com/2017/11/27/go-tools-and-gitlab-how-to-do-continuous-integration-like-a-boss/
PROJECT_NAME := "srandr"
PKG := "gitlab.com/ragon000/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test lint

all: build

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

dep: ## Get the dependencies
	@dep ensure

build: dep ## Build the binary file
	@go build -gccgoflags "-s -w" -compiler gccgo  -i -v $(PKG)/cmd/...

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

install: ## Installs build
	@mkdir -p $(DESTDIR)/usr/bin
	@cp $(PROJECT_NAME) $(DESTDIR)/usr/bin/

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
